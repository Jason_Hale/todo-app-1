// Help from Coach John Wilkinson and Micheal Trapnier
import React, { Component } from "react";
import todosList from "./todos.json";


class App extends Component {
  state = {
    todos: todosList,
  };
  clearCompleted = (event) => {
    let newToDos = this.state.todos.filter((todos)=> {
      return todos.completed === false
    })
    this.setState({todos: newToDos})
  }
handleComplete = (event, id) => {
  let newToDo = this.state.todos.map((todos) => {
    if(
      todos.id === id) {
        return {
          ...todos, completed: !todos.completed
        }
      }
      return todos
    })
    this.setState({todos: newToDo})
  }
    
  handleDelete = (event, id) => {
    const newToDo = this.state.todos.filter((todo)=> todo.id !== id);
    this.setState({todos: newToDo});
  }
  handleInputChange = event => {
    if (event.key === 'Enter'){
    const newToDo = {
        "userId": 1,
        "id": Math.floor(Math.random()*1000),
        "title": event.target.value,
        "completed": false
      }
      const newToDos = [...this.state.todos]
      newToDos.push(newToDo)
      this.setState({
        todos: newToDos,
      })
      event.target.value = null
    }
        
    };

    
    render() {
      return (
        <section className="todoapp">
          <header className="header">
            <h1>todos</h1>
            <input className="new-todo" placeholder="What needs to be done?" onKeyDown= {this.handleInputChange}autoFocus />
            
          </header>
          <TodoList todos={this.state.todos} handleDelete = {this.handleDelete} handleComplete= {this.handleComplete} />
          <footer className="footer">
            <span className="todo-count">
              <strong>0</strong> item(s) left
            </span>
            <button onClick= {this.clearCompleted} className="clear-completed">Clear completed</button>
          </footer>
        </section>
      );
    }
  }
  
  class TodoItem extends Component {
    render() {
      return (
        <li className={this.props.completed ? "completed" : ""}>
          <div className="view">
            <input className="toggle" type="checkbox" checked={this.props.completed} onChange ={this.props.handleComplete}/>
            <label>{this.props.title}</label>
            <button onClick={(event) => this.props.handleDelete(event, this.props.id)}className="destroy" />
          </div>
        </li>
      );
    }
  }
  
  class TodoList extends Component {
    render() {
      return (
        <section className="main">
          <ul className="todo-list">
            {this.props.todos.map((todo) => (
              <TodoItem title={todo.title} completed={todo.completed} handleDelete = {this.props.handleDelete}
              id ={todo.id} handleComplete = {(e)=>this.props.handleComplete(e, todo.id)}/>
              ))}
              </ul>
              </section>
              );
            }
  }
  
  export default App;
  
